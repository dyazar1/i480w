#!/usr/bin/env nodejs

'use strict';

const Ppm = require('./ppm');

const hex = require('./node_modules/hex-to-binary')
const hex2ascii = require('./node_modules/hex2ascii');

/** prefix which always precedes actual message when message is hidden
 *  in an image.
 */
const STEG_MAGIC = 'stg';

/** Constructor which takes some kind of ID and a Ppm image */
function StegModule(id, ppm) {
  this.id = id;
  this.ppm = ppm;
}

/** Hide message msg using PPM image contained in this StegModule object
 *  and return an object containing the new PPM image.
 *
 *  Specifically, this function will always return an object.  If an
 *  error occurs, then the "error" property of the return'd object
 *  will be set to a suitable error message.  If everything ok, then
 *  the "ppm" property of return'd object will be set to a Ppm image
 *  ppmOut which is derived from this.ppm with msg hidden.
 *
 *  The ppmOut image will be formed from the image contained in this
 *  StegModule object and msg as follows.
 *
 *    1.  The meta-info (header, comments, resolution, color-depth)
 *        for ppmOut is set to that of the PPM image contained in this
 *        StegModule object.
 *
 *    2.  A magicMsg is formed as the concatenation of STEG_MAGIC,
 *        msg and the NUL-character '\0'.
 *
 *    3.  The bits of the character codes of magicMsg including the
 *        terminating NUL-character are unpacked (MSB-first) into the
 *        LSB of successive pixel bytes of the ppmOut image.  Note
 *        that the pixel bytes of ppmOut should be identical to those
 *        of the image in this StegModule object except that the LSB of each
 *        pixel byte will contain the bits of magicMsg.
 *
 *  The function should detect the following errors:
 *
 *    STEG_TOO_BIG:   The provided pixelBytes array is not large enough 
 *                    to allow hiding magicMsg.
 *    STEG_MSG:       The image contained in this StegModule object may already
 *                    contain a hidden message; detected by seeing
 *                    this StegModule object's underlying image pixel bytes
 *                    starting with a hidden STEG_MAGIC string.
 *
 * Each error message must start with the above IDs (STEG_TOO_BIG, etc).
 */
StegModule.prototype.hide = function(msg) {
  //TODO: hide STEG_MAGIC + msg + '\0' into a copy of this.ppm
  //construct copy as shown below, then update pixelBytes in the copy.
  let ppmOut = new Ppm(this.ppm);
  // MODIFY COPYPPM TO WHATEVER, AND THEN RETURN IT IF THERE IS NO ERRORS
  let magicMsg = 'stg' + msg + '\0';
  let header = ppmOut.hdrBytes;
  let pixelData = ppmOut.pixelBytes;


  let i = 1;
  let binaryString ='';
  let char = '';
  let message = '';
  while (i-1<pixelData.length) {// LOOP THROUGH BYTES
    let value = pixelData[i - 1];
    if(isEven(value)){
      binaryString+='0';
    }else{
      binaryString+='1';
    }
    if(i % 8 === 0 ){ // if i is divisible by 8
      // convert binary string to hex
      let hex = parseInt(binaryString, 2).toString(16);
      char = hex2ascii(hex);
      if(binaryString=='00000000'){break}
      message += char;
      binaryString ='';
    }
    i++;
  }
  if( message.indexOf('stg') >= 0){
    return { ppm: null, error:'STEG_MSG: There is already a text hidden in this image'};
  }


  for(let i =0;i<magicMsg.length;i++){
    let char = magicMsg[i];
    let hex = char.charCodeAt();
    let binary = hex2bin(hex);
    // reach first 8 bytes
    for(let j =0;j<8;j++){
      if(!pixelData[i+j+2]){ // skipping the 0 and \ by adding +2
        return {ppm: null,error:'STEG_TOO_BIG: '}
      }
      let value = pixelData[i+j];
      let hex = value.toString(16);
      let binary = hex2bin(hex);
      let newBinary = binary.slice(0, -1) + binary[j];
      let newHex = parseInt(newBinary, 2).toString(16);
      let newValue = parseInt(newHex, 16);
      ppmOut.pixelBytes[i+j+2] = newValue; 
    }
  }


  return { ppm:ppmOut, error:false}
  

  


  // MAKE THE LEAST SIGNIFICANT BIT OF EACH PIXEL BYTE -> BITS OF magicMsg

  

/*


  if(){ //GELEN PPM IN IMAGE PIXEL BYTELARI STEG_MATIC STRINGIYLE BASLIYOSA.
    let errorMessage = 'STEG_MSG: ';
    return { msg: '', error:errorMessage };
  }else if(){
    let errorMessage = 'STEG_TOO_BIG: ';
    return { msg: '', error:errorMessage };
  }else{ // NO ERRORS
    return { ppm: ppmOut }; // RETURNING AN OBJECT CONTAINING a ppm
  }
*/

  
}

/** Return message hidden in this StegModule object.  Specifically, if
 *  an error occurs, then return an object with "error" property set
 *  to a string describing the error.  If everything is ok, then the
 *  return'd object should have a "msg" property set to the hidden
 *  message.  Note that the return'd message should not contain
 *  STEG_MAGIC or the terminating NUL '\0' character.
 *
 *  The function will detect the following errors:
 *
 *    STEG_NO_MSG:    The image contained in this Steg object does not
 *                    contain a hidden message; detected by not
 *                    seeing this Steg object's underlying image pixel
 *                    bytes starting with a hidden STEG_MAGIC
 *                    string.
 *    STEG_BAD_MSG:   A bad message was decoded (the NUL-terminator
 *                    was not found).
 *
 * Each error message must start with the above IDs (STEG_NO_MSG, etc).
 */
StegModule.prototype.unhide = function() {
  let ourPpm = this.ppm;
  let bytesOfTheImage = ourPpm.bytes();
  let pixelData = ourPpm.pixelBytes;
  let i = 1;
  let binaryString ='';
  let char = '';
  let message = '';
  let nullDataFound = false;
  while (i-1<pixelData.length) {// LOOP THROUGH BYTES
    let value = pixelData[i - 1];
    if(isEven(value)){
      binaryString+='0';
    }else{
      binaryString+='1';
    }
    if(i % 8 === 0 ){ // if i is divisible by 8
      // convert binary string to hex
      let hex = parseInt(binaryString, 2).toString(16);
      char = hex2ascii(hex);
      if(binaryString=='00000000'){nullDataFound=true;break}
      message += char;
      binaryString ='';
    }
    i++;
  }

  if(!nullDataFound){
    let errorMessage = 'STEG_BAD_MSG: ';
    return { msg: '', error:errorMessage};
  }

  if( message.indexOf('stg') >= 0){ //IF THERE IS PREFIX, REMOVE IT
    message = message.replace('stg','');
    return { msg: message, error:false};
  }else{
    let errorMessage = 'STEG_NO_MSG: ';
    return { msg: '', error:errorMessage};
  }
  
}
function isEven(n) {
  return n % 2 == 0;
}

function isOdd(n) {
  return Math.abs(n % 2) == 1;
}
function hex2bin(hex){
  return ("00000000" + (parseInt(hex, 16)).toString(2)).substr(-8);
}

function extractMagicMsg(bytes){

}

module.exports = StegModule;
