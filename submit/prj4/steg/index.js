const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const Steg = require('steg')
const Ppm = require('ppm')
var mustacheExpress = require('mustache-express');

var port = '';
var wsURL = '';

process.argv.forEach(function (val, index, array) {
    if(index==2){
        port = val;
    }
    if(index==3){
        wsURL = val;
    }
});

app.use(bodyParser.urlencoded({ extended: true }))
app.listen(port, function () {
    console.log('Example app listening on port 3000!')
})

app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache');



app.get('/hide', function (req, res) {

    axios.get(wsURL + '/listImages')
        .then(function (response) {
            var images = response;
            var imagesArray = Object.values(images);
            var imagesDiv = '';
            for (let i in imagesArray) {
                imagesDiv += '<img href="' + imagesArray.link + '"/>'
            }
            var messagesArray = ['message1', 'message2', 'message3'];
            var messagesToShow = '<option value="default">Not selected</option>';
            for (let j in messagesToShow) {
                messagesToShow += '<option value="' + messagesArray[j] + '">' + messagesArray[j] + '</option>';
            }
            res.render('hide', { images: imagesDiv, messages: messagesToShow });
        })
        .catch(function (error) {
            console.log(error);
        });

})

app.get('/index.html', function (req, res) {
    res.render('index') // showing the index page
})

app.post('/hideMessage', function (req, res) {

    var message = req.body.message;
    var messageFromFile = req.body.filepicker;
    var messageToUse = message;
    if (!message) {
        messageToUse = messageFromFile;
    }

    axios.post(wsURL + '/stegHide', messageToUse)
        .then(function (response) {
            var nameOfCreatedImage = response;
            res.render('hideSuccess', { imageName: nameOfCreatedImage });
        })
        .catch(function (error) {
            var errMsg = 'yarak';
            res.render('hide', { errorMessage: errMsg }); //redisplay same page with a suitable error message. 
        });
})

app.get('/unhide', function (req, res) {

    axios.get(wsURL + '/listImages')
        .then(function (response) {
            var images = response;
            var imagesArray = Object.values(images);
            var imagesDiv = '';
            for (let i in imagesArray) {
                imagesDiv += '<img href="' + imagesArray.link + '"/>'
            }
            res.render('unhide', { images: imagesDiv });
        })
        .catch(function (error) {
            console.log(error);
        });

})

app.post('/unhideMessage', function (req, res) {


    // var chosenImage = req.body.message;
    axios.post(wsURL + '/stegHide', messageToUse)
        .then(function (response) {
            var recoveredMessage = 'am';
            res.render('hideSuccess', { message: recoveredMessage });
        })
        .catch(function (error) {
            var errMsg = 'yarak';
            res.render('unhide', { errorMessage: errMsg }); //redisplay same page with a suitable error message. 
        });
})

